const ipBtn = document.querySelector(".ip-btn");
ipBtn.addEventListener("click", findByIp);


async function findByIp() {
    try {
        const ipResponse = await fetch("https://api.ipify.org/?format=json");
        const {ip} = await ipResponse.json();
        const addressResponse = await fetch(`http://ip-api.com/json/${ip}`);
        const address = await addressResponse.json();
        renderAddress(address);
    } catch (error) {
        console.log(error);
    }
}

function renderAddress({timezone, country, regionName, city, zip}) {
    const list = document.createElement("ul");
    const renderableAddress = {timezone, country, regionName, city, zip};
    for (const key in renderableAddress) {
        const item = document.createElement("li");
        item.textContent = `${key}: ${renderableAddress[key]}`;
        list.append(item);
    }
    const root = document.body.querySelector("#root");
    root.innerHTML = "";
    root.append(list);
}